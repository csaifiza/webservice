# Malicious URL Checker

A webservice that can recieves a url and checks if the provided url is malicious based on a database of information that the service is using as a resource to check.

## Technologies used in Repository

> Redis

> Golang

> Docker

> https://github.com/gorilla/mux

## Required Technologies To Install

> Docker

> Go

___

## Implementation Guide

> docker pull redis

While in the root directory:

> docker network create -d bridge malware-net

> docker-compose build

> docker-compose up

If this is the first time running the container with redis then after the previous commands:

> cd redis

> go run inserter.go

This will load the csv information into redis. The persistancy is on so redis will load this information after running the previous commands without needing to run inserter.go

___

## Teardown Guide 

In the root directory:

> docker-compose down

___

## lookup.go

This contains the code for the webservice used to check malicious data from within the redis database. There are two endpoints. One is for addresses that have no extra endpoints on the returned url and the other is for all other urls. After extracting the url without the endpoints and the parameters into a map there is a check to see if the url contained multiple parameters. if it did The url will call a sorting algorithm that uses a slice structure to sort the parameters in sorted order before returning the full url. A full url is used to check against the Redis keyvalue store to check if the key exists within the database and see if it is malicious. the result and the url are returned in a json to the client after determining maliciousness.

___

## inserter.go

inserts the information from the malwarelist.csv file into the redis database for first time setup. The service checks if an iteration has multiple parameters. if it does then the program will sort the parameters in alphabetical order and restructure the url before inserting.

___

## adapter.go

contains the get request that checks the current url with the redis database. I seperated the database code so that developers in the future can simply slip their own implementation of the function contained within the adapter.go code if they choose to use a different database.

___

## Redis

Redis is an in memory database used in this project. The domains are the keys while the value is the index of the csv at the time. getkey promises an O(n) time complexity based on the number of keys you are asking for. Since we are only fetching 1 key, that results in a O(1) complexity.

___

## Golang

The program is written in golang because of the of its speed and ease of use. It's a strong language for building servers in this regard. I used a package called gorilla/mux which gave me a more versatile router to use.

___

## Docker

Docker was used to easily setup the service quickly and to not require as many technologies to be run on the os as I can commit to. The compose file was designed with the mindset that this will be tested on one machine and it can easily be used for impementaion. However, If I were to seperate the components then I would use a completely different set of commands to run both of the docker containers/images and forgo using a docker network.

___

## malwarelist.csv

BEWARE these links to actual malicious urls. This csv was provided at: http://www.malwaredomainlist.com/mdlcsv.php