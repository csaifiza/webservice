module bitbucket.org/csaifiza/webservice/src/master

go 1.14

require (
	github.com/go-redis/redis v6.15.8+incompatible
	github.com/go-redis/redis/v8 v8.0.0-beta.5 // indirect
	github.com/gorilla/mux v1.7.4
)
