package main

import (
	"github.com/go-redis/redis"
)

/*
isMalware(string) bool
checks the redis keyvalue store to see if the url is an existing malware laden url
returns false if not found
returns true if found
*/
func IsMalware(fullURL string) bool {
	//sets up the client accessing the redis kv store
	client := redis.NewClient(&redis.Options{
		Addr:     "malwareDB:6379",
		Password: "",
		DB:       0,
	})
	//checking a valid key to see what it returns
	//storing index based on csv
	_, err := client.Get(fullURL).Result()
	if err == redis.Nil {
        return false
    } else if err != nil {
        panic(err)
    } else {
        return true
    }
}