/*
inserter.go
inserts the malware laden url from the csv into an easily retrievable key value store
on redis
*/
package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/go-redis/redis"
)

/*
ReadCsv(string)
opens the csv found here: http://www.malwaredomainlist.com/mdlcsv.php
stores the urls into a redis database
if the url has multiple parameters then
they are sorted before storing
*/
func ReadCsv(path string) {
	//path to csv
	f, _ := os.Open(path)
	r := csv.NewReader(f)
	i := 1
	//sets connection to redis
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		//record[1] contains the url in each csv
		oldURL := record[1]
		/*
			if the url contains more than one suffix the string will be split between the two
			and the suffixes will be split apart into a slice before being sorted and rejoined.
			then the suffix and prefix are rejoined as oldURL before being stored into the redis
		*/
		if strings.Contains(oldURL, "?") && strings.Contains(oldURL, "&") {
			//split between suffix and prefix
			splitURL := strings.Split(oldURL, "?")
			//split between queries
			suffix := strings.Split(splitURL[1], "&")
			sort.Strings(suffix)
			oldURL = splitURL[0] + "?" + strings.Join(suffix, "&")
		}
		//stores value in redis
		err = client.Set(oldURL, i, 0).Err()
		if err != nil {
			fmt.Println(err)
		}
		i = i + 1
	}

}

func main() {
	//get current directory
	dir, err := os.Getwd()
	//set path to csv
	dir = dir + "/malwarelist.csv"
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(dir)
	ReadCsv(dir)
}
