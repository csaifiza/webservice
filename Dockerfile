#getting base image
FROM golang:latest

MAINTAINER Cameron Saifizadeh

RUN mkdir /app

WORKDIR /app

COPY go.mod /app

COPY go.sum /app

COPY lookup.go /app

COPY adapter.go /app

RUN go get -u github.com/gorilla/mux

RUN go get github.com/go-redis/redis/v8

CMD ["go","run","lookup.go","adapter.go"]
