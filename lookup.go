package main

import (
	"encoding/json"
	"log"
	"net/http"
	"sort"

	"github.com/gorilla/mux"
)

/*
this is the payload that returns the information from the proxy about the malicious nature
of the url and the url name
*/
type payload struct {
	URL       string `json: "Url"`
	IsMalware bool   `json: "isMalware"`
}

func send(fullURL string, malware bool) payload {
	emp := payload{URL: fullURL, IsMalware: malware}
	return emp
}

/*
UrlChecker(w http.ResponseWriter, r *http.Request)
can handle a site with no extra parameters
*/
func UrlChecker(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	//build the url up to its queries
	fullURL := vars["url"]
	fullURL = Sorter(r, fullURL)
	malware := IsMalware(fullURL)
	data := send(fullURL, malware)
	ReturnToOrigin(data, w)
}

/*
Sorter(*http.Request, string) string
sorts the queries stored in the map for search parameters
will sort them alphabetically before returning a full url
with the parameters in alphabetical order
*/
func Sorter(r *http.Request, fullURL string) string {
	query := r.URL.Query()
	keys := make([]string, 0, len(query))
	if len(query) > 0 {
		fullURL = fullURL + "?"
		for k := range query {
			keys = append(keys, k)
		}
		//sort array with quicksort
		sort.Strings(keys)
		//build url with sorted queries
		for k := range keys {
			fullURL = fullURL + keys[k] + "=" + query[keys[k]][0]
			if k < len(keys)-1 {
				fullURL = fullURL + "&"
			}
		}
	}
	return fullURL

}

/*
UrlCheckerEndpoints(http.ResponseWriter, *http.Request)
handles endpoints that have multiple parameters using regular expressions.
regular expression matches everything to ?
*/
func UrlCheckerEndpoints(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	//build the url up to its queries
	fullURL := vars["url"] + "/" + vars["endpoint"]
	fullURL = Sorter(r, fullURL)
	malware := IsMalware(fullURL)
	data := send(fullURL, malware)
	ReturnToOrigin(data, w)
}

func ReturnToOrigin(data payload, w http.ResponseWriter) {
	pays, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.Write(pays)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/urlinfo/1/{url}", UrlChecker).Methods("GET")
	r.HandleFunc("/urlinfo/1/{url}/{endpoint:(?:[^\\?]+)(?:\\?.*)?}", UrlCheckerEndpoints).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", r))
}
